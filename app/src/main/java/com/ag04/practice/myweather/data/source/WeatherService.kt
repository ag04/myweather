package com.ag04.practice.myweather.data.source

import com.ag04.practice.myweather.data.model.CurrentWeather
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by akovar on 2019-09-04.
 */
interface WeatherService {

    @GET("weather")
    fun getCurrentWeatherByCoordinates(@Query("lon") lon: Int, @Query("lat") lat: Int, @Query("APPID") appId: String = APPID): Call<CurrentWeather>

    companion object {
        private val BASE_WEATHER_URL = "https://api.openweathermap.org/data/2.5/"
        private val APPID = "cca773ee3c174b2054007cc2091c7f5b"

        private val instance: WeatherService = Retrofit.Builder()
            .baseUrl(BASE_WEATHER_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(WeatherService::class.java)

        @Synchronized
        fun getInstance(): WeatherService {
            return instance
        }
    }
}