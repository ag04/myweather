package com.ag04.practice.myweather.component.home.currentweather

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ag04.practice.myweather.R
import com.ag04.practice.myweather.data.base.Resource
import com.ag04.practice.myweather.data.base.ResourceStatus
import com.ag04.practice.myweather.data.model.Coordinates
import com.ag04.practice.myweather.data.model.CurrentWeather
import kotlinx.android.synthetic.main.current_weather_fragment.*

class CurrentWeatherFragment : Fragment() {

    companion object {
        fun newInstance() =
            CurrentWeatherFragment()
    }

    private lateinit var viewModel: CurrentWeatherViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.current_weather_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(CurrentWeatherViewModel::class.java)
        viewModel.getCurrentWeather().observe(this, currentWeatherObserver)

//        // TODO set this from current device location
//        viewModel.provideCoordinates(Coordinates(lon = 15, lat = 45))

        timer.setOnClickListener {
            viewModel.provideCoordinates(Coordinates(lon = 17, lat = 44))
        }
    }

    /**
     * Update UI with when current weather changed.
     */
    private val currentWeatherObserver = Observer<Resource<CurrentWeather>> { resource ->
        // TODO set UI elements here
        when (resource.status) {
            ResourceStatus.LOADING -> message.text = "Loading.."
            ResourceStatus.SUCCESS -> message.text = resource.data!!.toString()
            else -> message.text = "Something went wrong!"
        }
    }
}
