package com.ag04.practice.myweather.data.source

import androidx.lifecycle.MutableLiveData
import com.ag04.practice.myweather.data.base.Resource
import com.ag04.practice.myweather.data.model.Coordinates
import com.ag04.practice.myweather.data.model.CurrentWeather
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

/**
 * Created by akovar on 2019-09-04.
 */
class WeatherRepository private constructor() {

    init {
        /**
         *  Every time init is called increment instance count
         *  just in case somehow we break singleton rule
         *  this will be called more than once and myInstancesCount > 1 == true
         */
        instanceCount++
    }

    companion object {
        private var instanceCount = 0
        private val instance = WeatherRepository()

        @Synchronized
        fun getInstance(): WeatherRepository {
            return instance
        }

        private val remoteWeatherDataSource: RemoteWeatherDataSource =
            RemoteWeatherDataSource.getInstance()
    }

    fun getCurrentWeatherByCoordinates(coordinates: Coordinates): MutableLiveData<Resource<CurrentWeather>> {
        val result = MutableLiveData<Resource<CurrentWeather>>()
        result.value = Resource.loading()

        CoroutineScope(IO).launch {
            try {
                val currentWeather =
                    remoteWeatherDataSource.getCurrentWeatherByCoordinates(coordinates)
                result.postValue(Resource.success(currentWeather))
            } catch (exception: Exception) {
                result.postValue(Resource.error("Something went wrong!"))
            }
        }

        return result
    }
}