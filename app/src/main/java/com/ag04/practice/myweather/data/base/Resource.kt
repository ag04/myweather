package com.ag04.practice.myweather.data.base

/**
 * Created by akovar on 2019-09-05.
 */

/**
 * Generic class that holds resource value with loading status.
 */
data class Resource<T> private constructor(
    val status: ResourceStatus,
    val data: T? = null,
    val message: String? = null
) {

    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(ResourceStatus.SUCCESS, data, null)
        }

        fun <T> loading(): Resource<T> {
            return Resource(ResourceStatus.LOADING, null, "Loading data...")
        }

        fun <T> error(message: String?): Resource<T> {
            return Resource(ResourceStatus.ERROR, null, message)
        }
    }
}

enum class ResourceStatus {
    LOADING, SUCCESS, ERROR
}