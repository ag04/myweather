package com.ag04.practice.myweather.component.home.currentweather

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.ag04.practice.myweather.data.base.Resource
import com.ag04.practice.myweather.data.model.Coordinates
import com.ag04.practice.myweather.data.model.CurrentWeather
import com.ag04.practice.myweather.data.source.WeatherRepository

class CurrentWeatherViewModel : ViewModel() {

    private val coordinates = MutableLiveData<Coordinates>()
    private val weatherRepository = WeatherRepository.getInstance()

    fun getCurrentWeather(): LiveData<Resource<CurrentWeather>> {
        return Transformations.switchMap(coordinates) { coordinates ->
            weatherRepository.getCurrentWeatherByCoordinates(coordinates)
        }
    }

    fun provideCoordinates(coordinates: Coordinates) {
        this.coordinates.value = coordinates
    }
}
