package com.ag04.practice.myweather.data.source

import com.ag04.practice.myweather.data.model.Coordinates
import com.ag04.practice.myweather.data.model.CurrentWeather
import java.io.IOException

/**
 * Created by akovar on 2019-09-05.
 */
class RemoteWeatherDataSource {

    companion object {

        private val instance = RemoteWeatherDataSource()
        private val weatherService = WeatherService.getInstance()

        @Synchronized
        fun getInstance(): RemoteWeatherDataSource {
            return instance
        }
    }

    fun getCurrentWeatherByCoordinates(coordinates: Coordinates): CurrentWeather {
        val response = weatherService.getCurrentWeatherByCoordinates(
            lon = coordinates.lon,
            lat = coordinates.lat
        ).execute()

        if (response.isSuccessful) {
            return response.body()!!
        } else {
            // TODO handle this case
            throw IOException()
        }
    }
}