package com.ag04.practice.myweather.component.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.ag04.practice.myweather.R
import com.ag04.practice.myweather.component.home.currentweather.CurrentWeatherFragment

/**
 * This is activity which holds main content.
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        // set content fragment to container
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, CurrentWeatherFragment.newInstance())
                .commitNow()
        }
    }
}
