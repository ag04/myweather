package com.ag04.practice.myweather.data.model

import com.google.gson.annotations.SerializedName

/**
 * Created by akovar on 2019-08-27.
 */
data class CurrentWeather(

    @SerializedName("coord")
    val coordinates: Coordinates? = null,

    @SerializedName("weather")
    val weather: List<Weather>? = null,

    @SerializedName("main")
    val airState: AirState? = null,

    @SerializedName("wind")
    val wind: Wind? = null,

    @SerializedName("clouds")
    val clouds: Clouds? = null,

    @SerializedName("sys")
    val sys: Sys? = null,

    /**
     * Time of data calculation, unix, UTC
     */
    @SerializedName("dt")
    val dt: Long? = null,

    /**
     * Shift in seconds from UTC
     */
    @SerializedName("timezone")
    val timezone: Long? = null,

    /**
     * City ID
     */
    @SerializedName("id")
    val id: String? = null,

    /**
     * City name
     */
    @SerializedName("name")
    val name: String? = null
)

data class Coordinates(

    /**
     * City geo location, longitude
     */
    @SerializedName("lon")
    val lon: Int,

    /**
     * City geo location, latitude
     */
    @SerializedName("lat")
    val lat: Int
)

data class Weather(

    /**
     * Weather condition id
     */
    @SerializedName("id")
    val id: Int,

    /**
     * Group of weather parameters (Rain, Snow, Extreme etc.)
     */
    @SerializedName("main")
    val main: String,

    /**
     * Weather condition within the group
     */
    @SerializedName("description")
    val description: String,

    /**
     * Weather icon id
     */
    @SerializedName("icon")
    val icon: String
)

data class AirState(

    /**
     * Temperature. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
     */
    @SerializedName("temp")
    val currentTemperature: Double,

    /**
     * Atmospheric pressure (on the sea level, if there is no sea_level or grnd_level data), hPa
     */
    @SerializedName("pressure")
    val pressure: Double,

    /**
     * Humidity, %
     */
    @SerializedName("humidity")
    val humidity: Int,

    /**
     *  Minimum temperature at the moment. This is deviation from current temp that is possible
     *  for large cities and megalopolises geographically expanded (use these parameter optionally).
     *  Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
     */
    @SerializedName("temp_min")
    val minTemperature: Double,

    /**
     * Maximum temperature at the moment. This is deviation from current temp that is possible
     * for large cities and megalopolises geographically expanded (use these parameter optionally).
     * Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
     */
    @SerializedName("temp_max")
    val maxTemperature: Double,

    /**
     * Atmospheric pressure on the sea level, hPa
     */
    @SerializedName("sea_level")
    val seaLevelPressure: Double,

    /**
     * Atmospheric pressure on the ground level, hPa
     */
    @SerializedName("grnd_level")
    val groundLevelPressure: Double
)

data class Wind(

    /**
     * Wind speed. Unit Default: meter/sec, Metric: meter/sec, Imperial: miles/hour.
     */
    @SerializedName("speed")
    val speed: Double,

    /**
     *  Wind direction, degrees (meteorological)
     */
    @SerializedName("deg")
    val degrees: Double
)

data class Clouds(

    /**
     * Cloudiness, %
     */
    @SerializedName("all")
    val all: Int
)

data class Rain(

    /**
     * Rain volume for the last 1 hour, mm
     */
    @SerializedName("1h")
    val lastOneHour: Int,

    /**
     * Rain volume for the last 3 hours, mm
     */
    @SerializedName("3h")
    val lastThreeHours: Int
)

data class Snow(

    /**
     * Snow volume for the last 1 hour, mm
     */
    @SerializedName("1h")
    val lastOneHour: Int,

    /**
     * Snow volume for the last 3 hours, mm
     */
    @SerializedName("3h")
    val lastThreeHours: Int

)

data class Sys(

    /**
     * Country code (GB, JP etc.)
     */
    @SerializedName("country")
    val country: String,

    /**
     * Sunrise time, unix, UTC
     */
    @SerializedName("sunrise")
    val sunrise: Long,

    /**
     * Sunset time, unix, UTC
     */
    @SerializedName("sunset")
    val sunset: Long
)
